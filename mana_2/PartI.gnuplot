
set terminal x11
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 3 ps 1
set style line 2 lc rgb '#00FFFF' lt 1 lw 2 pt 6 ps 1


#baristochrone
#set parametric
#set trange[0:pi]
#end baristo

qi=0.0
qf=5.0
T=1.5

#axis labels
set xlabel "t (s)"
set ylabel "x (m)" #rotate by  90 center

#queda dos corpos, campo gravitico constante
b(x)=-0.5*g*x**2+c1*x+qi
g=10.0
c1=(qf-qi)/T+0.5*g*T

#oscilador harmónico
c(x)=A*cos(W*x)+B*sin(W*x)
K=1.0
M=1.0
A=qi
W=sqrt(K/M)
B=(qf-qi*cos(W*T))/sin(W*T)

#desoscilador harmónico (não se resolve como as outras)
d(x)=A*exp(-E*x)
E=1.0
set xrange[0:T]

#brachistochrone
k=sqrt(1)
#r(t)=0.5*k**2*(t-sin(t))
#o(t)=1-0.5*k**2*(1-cos(t))


unset key

plot "mana.txt"  ls 1,\
#     o(t),r(t) notitle ls 2

pause -1

set terminal png
set output "mana.png"
replot
