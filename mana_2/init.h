#define MAX_IT 1000000
#define DIV    100
#define TOL    2e-6
#define R      1

#define QI     1.0
#define QF     20.0
#define T      80.0

#define M      1.0
#define M2     1.0
#define G      10.0
#define K      1.0
