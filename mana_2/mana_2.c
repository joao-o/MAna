#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_rng.h>
#include "mana_2.h"
#include "init.h"

#define M_PI   3.14159265358979323846264338327
#define DT     (T/DIV)

//rng
gsl_rng *r;

double lag(double q, double dq, double t)       //lagrangean, enable only one at a time
{
   (void) t;                   //declares t as unused
   //(void) q;
   //return sqrt((1+dq*dq)/(2*G*q));
   //return 0.5 * M * dq * dq - q * G * M;
   //free fall, constant gravitic field
   //return 0.5 * M * dq * dq - K * q * q * 0.5;
   //harmonic oscilator
   return 0.5 * M * dq * dq + G * M * M2 / q; 
   //free fall, gravitic field created by a point mass
   return M * dq * dq + K * q * q;
   //disharmonic oscilator
   //return sqrt((dq*dq+1)/(2*G*t));
   //brachistochrone curve
}

double action(double *q, int init, int fin)
//integrate the lagrangean in respect to time, between init and fin
{
   int i;
   double t, action;
   action = 0;
   t=init*DT+DT/2;
   for (i = init; i < fin; i++, t += DT) {
      action += lag((q[i] + q[i + 1]) / 2, (q[i + 1] - q[i]) / DT, t) * DT;
   }
   return action;
}

void printcfg(FILE * fp, double *q)
//print a configuration
{
   int i;
   double t = 0;
   for (i = 0; i < DIV + 1; i++, t += DT)
      fprintf(fp, "%f %f\n", t, q[i]);
}

long unsigned urand(void)
//fetches a random number form the system's entropy pool
{
   long unsigned int rand;
   FILE *fp;
   fp = fopen("/dev/urandom", "r");
   if (fread((char *) (&rand), sizeof(rand), 1, fp) != 1)
      rand = 0;
   fclose(fp);
   return rand;
}

void diff_action(double *q,double *result)
{
   const double h=1.47e-8;
   double save, oaction;
   int i;
   for(i=1;i<DIV;i++){
      save=q[i];
      oaction=action(q,i-1,i+1);
      q[i]=q[i]+h*q[i];
      result[i]=(action(q,i-1,i+1)-oaction)/(h*save);
      q[i]=save;
   }
}

void p_diff_action(FILE *f,double *q,const char* msg)
{ 
   int i;
   double diff[DIV+1];
   diff_action(q,diff);
   fprintf(f,msg);
   for (i = 0; i < DIV + 1; i++)
      fprintf(f,"%f\n",diff[i]);
}

int main(void)
{
   r = gsl_rng_alloc(gsl_rng_taus2);
   gsl_rng_set(r, urand());

   FILE *out, *log;
   out = fopen("mana.txt", "w");        //output file for plotting
   log = fopen("mana.log", "w");        //output file for plotting
   int i;

   double q[DIV + 1];

   for (i = 0; i < DIV + 1; i++)
      q[i] = (QF - QI) / DIV * i + QI;  //assume the path is a straight line initially
  
   p_diff_action(log,q,"***diff_init\n");

   int iter = 0;
   double radius = R;
   double naction = 0;
   double oaction = 0;          //new action and old action, variables for radious optimization
   double nlag = 0;
   double olag = 0;             // new lagrangean and ond 
   double s = 0;
   int press=1;
   double oldaction=action(q,0,DIV+1);
   do {
      iter++;
      for (i = 1; i < DIV; i++) {
         //attempt to minimize the action for every interval
         olag = action(q, i - 1, i + 1);
         s = (gsl_rng_uniform(r) - 0.5) * radius;
         q[i] += s;
         nlag = action(q, i - 1, i + 1);
         if (nlag > olag)
            q[i] -= s;
      }
      naction = action(q, 0, DIV);
      if (naction > oaction - radius*radius) {
         //modify the radius for faster convergence/ more precision
         radius *= 0.9;
         if (radius < TOL)
           break;
      } else {
         radius *= 1.1;
         if (!isfinite(radius))
            break;
      }
      oaction = naction;
      if(iter==press)
        {
          printcfg(out, q);
          press+=200;
          }
   } while (iter < MAX_IT);

   printf("rad: %.2g\n", radius);
   printf("its: %d\n", iter);
   double finact=action(q,0,DIV+1);
   printf("oacc: % f\n acc: %f\n", oldaction, finact);
   printcfg(out, q);
   
   //p_diff_action(log,q,"****diff_fin\n");

   fclose(out);
   fclose(log);
   gsl_rng_free(r);
   return 0;
}
